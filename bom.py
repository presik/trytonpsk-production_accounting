# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from datetime import datetime, date
from trytond.pyson import Eval, If, Bool
from trytond.pool import Pool, PoolMeta
from trytond.modules.product import price_digits, round_price
from trytond.model import fields, ModelSQL, ModelView


class BOM(metaclass=PoolMeta):
    __name__ = 'production.bom'
    direct_costs = fields.One2Many('production.bom.direct_cost', 'bom',
        'Direct Costs')
    is_subproduction = fields.Boolean('Is Subproduction')
    not_synchronize_sales = fields.Boolean('Not Synchronize Sales')
    cost_estimate = fields.Function(fields.Numeric('Cost estimate',
        digits=price_digits),'get_cost_estimate')
    sale_price = fields.Function(fields.Numeric('Sale Price',
        digits=price_digits), 'get_sale_price')
    profit = fields.Function(fields.Numeric('Profit', digits=(16, 2)), 'get_profit')

    def get_cost_estimate(self, name=None):
        cost_estimate = 0
        Uom = Pool().get('product.uom')
        output_quantity = Decimal(self.outputs[0].quantity if self.outputs else 0)
        for input in self.inputs:
            internal_quantity = Uom.compute_qty(
                input.unit, input.quantity, input.product.default_uom, round=False)
            cost_estimate += (input.product.template.cost_price or 0) * Decimal(internal_quantity)
        cost_estimate += ((sum(dc.total_amount for dc in self.direct_costs if dc.total_amount)) * output_quantity) or 0
        return round(cost_estimate, price_digits[1])

    def get_sale_price(self, name=None):
        sale_price = sum([output.product.template.list_price for output in self.outputs])
        return round(sale_price, price_digits[1])

    def get_profit(self, name=None):
        cost_price = self.cost_estimate
        list_price = self.sale_price
        profit = (1 - (cost_price / list_price)) * 100 if list_price > 0 else 0
        return round(profit, 2)

    @classmethod
    def calc_cost_ldm(cls):
        # This method is temporal for CRON sync for a specific customer
        ProductLdm = Pool().get('production.bom')
        ProductLdmOut = Pool().get('production.bom.output')
        Product = Pool().get('product.product')
        Uom = Pool().get('product.uom')

        current_date = datetime.combine(date.today(), datetime.min.time())
        ldm_outs = ProductLdmOut.search_read([], fields_names=['id', 'product'])
        products_ldm = [p['product'] for p in ldm_outs]
        products = Product.search([
            ('active', '=', True),
            ('template.producible', '=', True),
            ('write_date', '<', current_date),
            ('id', 'in', products_ldm)
        ], limit=30)
        prd_ids = [p.id for p in products]
        products_ldm = ProductLdm.search([
            ('active', '=', True),
            ('output_products', 'in', prd_ids)
        ])
        uom_compute_qty = Uom.compute_qty
        for ldm in products_ldm:
            output = ldm.outputs[0]
            if not ldm.outputs or not ldm.inputs:
                continue
            cost = ldm.get_cost_ldm(uom_compute_qty)
            if cost > 0:
                cost = round(cost / Decimal(output.quantity), 4)
                Product.write([output.product], {'cost_price': cost})

    def compute_unit_cost(self):
        Product = Pool().get('product.product')
        total_cost = self.get_cost_ldm()
        cost = 0
        for output in self.outputs:
            if output.quantity > 0:
                cost = round_price(total_cost / Decimal(output.quantity))
                Product.write([output.product], {'cost_price': cost})
        return cost

    def get_cost_ldm(self):
        Uom = Pool().get('product.uom')
        compute_qty = Uom.compute_qty
        res = []
        for input_ in self.inputs:
            quantity = compute_qty(
                    input_.unit, input_.quantity,
                    input_.product.default_uom)
            cost_price = input_.product.cost_price
            res.append(Decimal(str(quantity)) * cost_price)

        return round_price(sum(res))

    def _group_purchase_key(self):
        supplier = self.supplier
        if self.routing.supplier_service_supplier:
            currency = self.routing.supplier_service_supplier.currency
        else:
            currency = self.company.currency
        return (
            ('company', self.company),
            ('party', supplier),
            ('payment_term', supplier.supplier_payment_term),
            ('warehouse', self.warehouse),
            ('currency', currency),
            ('invoice_address', supplier.address_get(type='invoice')),
            )


class BOMDirectCost(ModelSQL, ModelView):
    "BOMDirectCost"
    __name__ = "production.bom.direct_cost"
    bom = fields.Many2One('production.bom', 'BOM', required=True,
        ondelete='CASCADE')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[('type', '!=', 'active')])
    product_uom_category = fields.Function(
        fields.Many2One('product.uom.category', 'Product Uom Category'),
        'on_change_with_product_uom_category')
    unit = fields.Many2One('product.uom', 'UoM',
        domain=[
            If(Bool(Eval('product_uom_category')),
                ('category', '=', Eval('product_uom_category')),
                ('category', '!=', -1)),
            ],
        depends=['product', 'product_uom_category'])
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_unit_digits')
    effective_date = fields.Date('Effective Date')
    quantity = fields.Float('Quantity', required=True, digits=(16, Eval('unit_digits', 2)),
        depends=['unit_digits'])
    unit_price = fields.Numeric('Unit Price', required=True)
    total_amount = fields.Function(fields.Numeric('Amount', digits=price_digits),
                                   'get_total_amount')
    # amount = fields.Numeric('Amount', digits=price_digits,
        # states={
            # 'readonly': True,
            # 'required': True,
        # })
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
            # ('parent', '=', Eval('_parent_production', {}).get('analytic_account')),
        ])
    kind = fields.Selection([
        ('labour', 'Labour'),
        ('indirect', 'Indirect'),
        ('outsourcing', 'Outsourcing'),
        ('service', 'Service'),
        ], 'Kind', required=True)
    notes = fields.Text('Notes')
    account_move = fields.Many2One('account.move', 'Account Move',
        states={'readonly': True})
    supplier = fields.Many2One('party.party', "Supplier",
        states={'invisible': Eval('kind') != 'outsourcing',
                'required': Eval('kind') == 'outsourcing'},
        )

    def _group_purchase_key(self, company, warehouse):
        supplier = self.supplier
        return (
            ('company', company),
            ('party', supplier),
            ('payment_term', supplier.supplier_payment_term),
            ('warehouse', warehouse),
            ('currency', company.currency),
            ('invoice_address', supplier.address_get(type='invoice')),
            )

    @fields.depends('kind', 'product', 'supplier', 'unit', 'unit_price')
    def on_change_kind(self, name=None):
        if self.kind != 'outsourcing':
            self.unit = None
            self.unit_price = None
            self.supplier = None
            self.product = None

    @fields.depends('product', 'unit', 'unit_price', 'unit_digits')
    def on_change_product(self, name=None):
        if self.product:
            category = self.product.default_uom.category
            if not self.unit or self.unit.category != category:
                self.unit = self.product.default_uom.id
                self.unit_digits = self.product.default_uom.digits
            self.unit_price = self.product.cost_price
            # self.amount = Decimal(0)

    def get_total_amount(self, name=None):
        if self.product and self.quantity:
            return self.unit_price * Decimal(self.quantity)

    # @fields.depends('unit_price', 'amount', 'quantity')
    # def on_change_quantity(self, name=None):
    #     if self.unit_price and self.quantity:
    #         self.amount = round_price(Decimal(self.unit_price) * Decimal(self.quantity))

    @fields.depends('unit')
    def on_change_with_unit_digits(self, name=None):
        if self.unit:
            return self.unit.digits
        return 2

    @fields.depends('product')
    def on_change_with_product_uom_category(self, name=None):
        if self.product:
            return self.product.default_uom_category.id

    # @staticmethod
    # def default_amount():
    #     return 0
