# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import production
from . import account
from . import stock
from . import ir
from . import bom
from . import configuration
from . import sale
from . import log_production


def register():
    Pool.register(
        configuration.Configuration,
        production.Production,
        production.ProductionDetailedStart,
        production.ProcessProductionAsyncStart,
        production.ProductionCost,
        production.SubProduction,
        production.SubProductionIn,
        # production.UpdateCostProductionStart,
        # production.RoutingStep,
        account.Move,
        stock.Move,
        ir.Cron,
        bom.BOM,
        bom.BOMDirectCost,
        sale.SaleLine,
        module='production_accounting', type_='model')
    Pool.register(
        log_production.LogProductionAsync,
        module='production_accounting', type_='model',
        depends=['sale_shop'])
    Pool.register(
        production.UpdateCostProduction,
        production.ProductionForceDraft,
        production.ProductionDetailed,
        production.ProcessProductionAsync,
        production.DoneProductions,
        sale.CreateProductionFromSale,
        module='production_accounting', type_='wizard')
    Pool.register(
        production.ProductionDetailedReport,
        production.ProductionReport,
        module='production_accounting', type_='report')
