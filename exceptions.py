# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.exceptions import UserError
from trytond.modules.account.exceptions import (CancelWarning)
# from trytond.model.exceptions import ValidationError


class ProductionForceDraftAccountMoves(CancelWarning):
    pass


class ProductionExplodeMoveValues(UserError):
    pass


class ProductionDelete(UserError):
    pass


class ProductionQuantityValue(UserError):
    pass

class ProductionAccountMove(UserError):
    pass
