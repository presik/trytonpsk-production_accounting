# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool

from trytond.modules.account_product.product import account_used

account_names = ['account_stock', 'account_production']

# Moved to stock_co
# class ProductAverageCost(ModelSQL, ModelView):
#     "Product Average Cost"
#     __name__ = 'product.average_cost'
#     product = fields.Many2One('product.product', 'Product', readonly=True)
#     date = fields.Date('Date', readonly=True)
#     cost_price = fields.Numeric('Cost Price', digits=(16, 2), readonly=True)


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'
    account_production = fields.MultiValue(fields.Many2One('account.account',
        'Account Production', domain=[
            ('closed', '!=', True),
            ('type.stock', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ],
        states={
            'invisible': (~Eval('context', {}, ).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
        },
        depends=['account_parent', 'accounting']))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in account_names:
            return pool.get('product.category.account')
        return super(Category, cls).multivalue_model(field)

    @property
    @account_used('account_production')
    def account_production_used(self):
        pass

    @property
    @account_used('account_stock')
    def account_stock_used(self):
        pass

    @fields.depends('accounting', 'account_stock')
    def on_change_accounting(self):
        super().on_change_accounting()
        if not self.accounting:
            self.account_production = None


class CategoryAccount(metaclass=PoolMeta):
    __name__ = 'product.category.account'
    account_production = fields.Many2One(
        'account.account', "Account Production",
        domain=[
            ('closed', '!=', True),
            ('type.expense', '=', True),
            ('company', '=', Eval('company', -1)),
        ],
        depends=['company'])


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @property
    @account_used('account_production', 'account_category')
    def account_production_used(self):
        pass


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    account_production_used = fields.Function(fields.Many2One(
        'account.account', "Account COGS"), 'get_account_stock_category')

    def get_account_stock_category(self, name):
        if self.account_category:
            if name == 'account_production_used':
                return self.account_category.account_production.id
