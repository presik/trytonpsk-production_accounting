# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date, datetime
from decimal import Decimal

from sql import Table
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, fields
from trytond.modules.company import CompanyReport
from trytond.modules.product import price_digits, round_price
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, If
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

from .exceptions import *

# from rich.table import Table
# from rich import print
# from rich.console import Console


ZERO = Decimal(0)
BOM_CHANGES = ['bom', 'product', 'quantity', 'uom', 'warehouse', 'location',
    'company', 'inputs', 'outputs']


def round_dec(number):
    if not isinstance(number, Decimal):
        number = Decimal(number)
    return Decimal(number.quantize(Decimal('.01')))


class SubProduction(ModelSQL, ModelView):
    "Sub Production"
    __name__ = "production.sub"
    production = fields.Many2One('production', 'Production', required=True,
        ondelete='CASCADE')
    product = fields.Many2One('product.product', 'Product', required=True)
    effective_date = fields.Date('Effect. Date', required=True)
    quantity = fields.Float('Quantity', required=True)
    inputs = fields.One2Many('production.sub.in', 'sub', 'Sub. Inputs')
    employee = fields.Many2One('company.employee', 'Employee')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('in_progress', 'In progress'),
            ('finished', 'Finished'),
            ('cancelled', 'Canceled'),
        ], 'State')
    notes = fields.Char('Notes')

    @staticmethod
    def default_effective_date():
        return date.today()


class SubProductionIn(ModelSQL, ModelView):
    "Sub Production In"
    __name__ = "production.sub.in"
    sub = fields.Many2One('production.sub', 'Production Sub', required=True,
        ondelete='CASCADE')
    product = fields.Many2One('product.product', 'product', required=True)
    move = fields.Many2One('stock.move', 'Move')
    quantity = fields.Float('Quantity', required=True)
    notes = fields.Char('Notes')


class Production(metaclass=PoolMeta):
    __name__ = 'production'
    _analytic_dom = [
        ('type', 'in', ['normal', 'distribution']),
        ('company', '=', Eval('context', {}).get('company', -1)),
        ('parent', '=', Eval('analytic_account')),
    ]
    _states = {
        'readonly': ~Eval('state').in_(['draft', 'request']),
    }
    in_account_move = fields.Many2One('account.move', 'In Account Move',
        states={'readonly': True})
    out_account_move = fields.Many2One('account.move', 'Out Account Move',
        states={'readonly': True})
    # warehouse_origin = fields.Many2One('stock.location', 'Warehouse Origin',
    #     domain=[('type', '=', 'warehouse')], states={'required': True})
    # warehouse_target = fields.Many2One('stock.location', 'Warehouse Target',
        # domain=[('type', '=', 'storage')], states={'required': True})
    warehouse_moves = fields.One2Many('stock.move', 'origin', 'Warehouse Moves')
    subs = fields.One2Many('production.sub', 'production', 'Sub Productions')
    costs = fields.One2Many('production.cost', 'production', 'Costs')
    material_costs = fields.Numeric('Material Costs', digits=price_digits,
        states={'readonly': True})
    labour_costs = fields.Numeric('Labour Costs', digits=price_digits,
        states={'readonly': True})
    indirect_costs = fields.Numeric('Indirect Costs', digits=price_digits,
        states={'readonly': True})
    services_costs = fields.Numeric('Services Costs', digits=price_digits,
        states={'readonly': True})
    total_cost = fields.Function(fields.Numeric('Total Cost', digits=price_digits),
                                 'get_total_cost')
    performance = fields.Function(fields.Numeric('Performance', digits=(16, 2)),
        'get_performance')
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', '=', 'normal'),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ], states=_states)
    analytic_account_materials = fields.Many2One('analytic_account.account',
        'Analytic Account Materials')
        # 'Analytic Account Materials', domain=_analytic_dom)
    analytic_account_labour = fields.Many2One('analytic_account.account',
        'Analytic Account Labour')
        # 'Analytic Account Labour', domain=_analytic_dom)
    analytic_account_indirect = fields.Many2One('analytic_account.account',
        'Analytic Account Indirect')
        # 'Analytic Account Indirect', domain=_analytic_dom)
    analytic_account_services = fields.Many2One('analytic_account.account',
        'Analytic Account Services')
        # 'Analytic Account Services', domain=_analytic_dom)
    subproductions = fields.One2Many('production.sub', 'production',
        'Sub-Productions')
    pre_productions = fields.Function(fields.One2Many('production', None, 'Pre-Productions'), 'get_productions')
    account_production = fields.Many2One('account.account',
        'Account Production', domain=[
            ('closed', '!=', True),
            ('type.stock', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])

    @classmethod
    def delete(cls, productions):
        for production in productions:
            if production.number:
                raise ProductionDelete(
                    gettext('production_accounting.msg_production_delete'))
        super(Production, cls).delete(production)

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls.bom.states = {
            'readonly': ~Eval('planned_date'),
        }
        cls._buttons.update({
            'generate_sub': {
                'invisible': Eval('state').in_(['done']),
            },
            'post_production': {
                'invisible': Eval('state').in_(['done']),
            },
            'generate_pre_productions': {
                'invisible': ~Eval('state').in_(['assigned']),
            },
        })

    def get_cost(self, name):
        # cost = super(Production, self).get_cost(name)
        cost = Decimal(0)
        for input_ in self.inputs:
            if input_.unit_price is not None:
                cost_price = input_.unit_price
            else:
                cost_price = input_.product.cost_price
            cost += (Decimal(str(input_.internal_quantity)) * cost_price)
        indirect_costs = self.indirect_costs or 0
        services_costs = self.services_costs or 0
        labour_costs = self.labour_costs or 0
        cost = cost + indirect_costs + services_costs + labour_costs
        return cost

    @staticmethod
    def default_material_costs():
        return 0

    @staticmethod
    def default_labour_costs():
        return 0

    @staticmethod
    def default_indirect_costs():
        return 0

    @staticmethod
    def default_services_costs():
        return 0

    def _explode_move_values(self, type, bom_io, quantity):
        move = super(Production, self)._explode_move_values(type, bom_io, quantity)
        if type == 'input' and self.planned_date:
            pool = Pool()
            AverageCost = pool.get('product.average_cost')
            Uom = pool.get('product.uom')
            domain = [('product', '=', bom_io.product.id), ('effective_date', '<=', self.planned_date)]
            average_cost = AverageCost.search(domain, order=[('effective_date', 'DESC')])
            unit_price = average_cost[0].cost_price if len(average_cost) > 0 else bom_io.product.cost_price
            move.unit_price = round(Uom.compute_price(
                    move.product.default_uom, unit_price, move.unit), 4)
        return move

    @classmethod
    def get_origin(cls):
        origins = super(Production, cls).get_origin()
        return origins + [('sale.line', 'Sale Line')]

    # @fields.depends(methods=['create_cost'])
    # def on_change_bom(self):
    #     super(Production, self).on_change_bom()
    #     print('fuck')
    #     pass
        # self.costs = self.create_cost()
        # self.on_change_costs()

    @classmethod
    @ModelView.button
    def generate_sub(cls, records):
        pool = Pool()
        BOMOutput = pool.get('production.bom.output')
        Sub = pool.get('production.sub')
        to_create = []
        for pcc in records:
            for input in pcc.inputs:
                outputs = BOMOutput.search([
                    ('product', '=', input.product.id),
                    ('bom.is_subproduction', '=', True),
                ])
                if not outputs:
                    continue

                output = outputs[0]
                to_create = {
                    'production': pcc.id,
                    'product': input.product.id,
                    'quantity': input.quantity,
                    'state': 'draft',
                }

                _inputs_to_create = []
                for _input in output.bom.inputs:
                    _inputs_to_create.append({
                        'product': _input.product.id,
                        'quantity': _input.quantity * input.quantity,
                    })
                to_create['inputs'] = [('create', _inputs_to_create)]

                if to_create:
                    Sub.create([to_create])

    @classmethod
    @ModelView.button
    def generate_pre_productions(cls, records):
        pool = Pool()
        BOMOutput = pool.get('production.bom.output')
        Production = pool.get('production')
        Bom = pool.get('production.bom')
        for pcc in records:
            if len(pcc.pre_productions) > 0:
                continue
            productions = []
            for input in pcc.inputs:
                outputs = BOMOutput.search([
                    ('product', '=', input.product.id),
                    ('bom.is_subproduction', '=', True),
                ])
                quantity = pcc.quantity_to_produce(input.quantity, input.current_stock)
                if len(outputs) == 0 or quantity == 0:
                    continue
                output = outputs[0].bom.inputs[0]
                bom = Bom.search([('output_products', '=', input.product.id)])
                productions.append({
                    'company': pcc.company.id,
                    'warehouse': pcc.warehouse.id,
                    'location': pcc.warehouse.production_location.id,
                    'planned_date': pcc.planned_date,
                    'planned_start_date': pcc.planned_start_date,
                    'effective_date': pcc.effective_date,
                    'effective_start_date': pcc.effective_start_date,
                    'product': input.product.id,
                    'uom': input.unit.id,
                    'quantity': quantity,
                    'state': 'draft',
                    'bom': bom[0].id if len(bom) > 0 else None,
                    'origin': str(pcc),
                    'outputs':  [('create', [{
                        'product': output.product.id,
                        'quantity': output.quantity,
                        'unit': output.unit.id,
                        'unit_price': output.product.cost_price,
                        'from_location': pcc.warehouse.production_location.id,
                        'to_location': pcc.warehouse.storage_location.id,
                    }])],
                })
            result = Production.create(productions)
            for r in result:
                r.explode_bom()
                r.save()

    def quantity_to_produce(self, quantity, current_stock):
        if current_stock > 0:
            if current_stock > quantity:
                return 0
            else:
                return abs(current_stock - quantity)
        else:
            return quantity

    @classmethod
    @ModelView.button
    def post_production(cls, records):
        cls.create_account_move(records, 'partial')

    # @staticmethod
    # def default_warehouse_origin():
    #     config = Pool().get('production.configuration')(1)
    #     if config.warehouse_origin:
    #         return config.warehouse_origin.id
    #     return

    # @staticmethod
    # def default_warehouse_target():
    #     config = Pool().get('production.configuration')(1)
    #     if config.warehouse_target:
    #         return config.warehouse_target.id
    #     return

    @classmethod
    def wait(cls, productions):
        pool = Pool()
        Config = pool.get('production.configuration')
        config = Config.get_configuration()
        if config.production_accounting:
            cls.create_account_move(productions, 'wait')
        cls.create_purchase(productions)
        super(Production, cls).wait(productions)

    @classmethod
    def run(cls, records):
        super(Production, cls).run(records)
        for rec in records:
            rec.create_stock_move_sub()

    @classmethod
    def done(cls, records):
        pool = Pool()
        Move = pool.get('stock.move')
        super(Production, cls).done(records)
        Config = pool.get('production.configuration')
        config = Config.get_configuration()
        if config.production_accounting:
            cls.create_account_move(records, 'done', 'out_account_move')
            for rec in records:
                for output in rec.outputs:
                    new_cost = rec._compute_unit_cost(output)
                    # unit_price = round_price(rec.cost / Decimal(output.quantity))
                    Move.write([output], {'unit_price': new_cost})
                    rec.update_product_cost(output.product, new_cost)
                    rec.product.cost_price = new_cost
                    rec.product.save()

    def get_productions(self, name=None):
        Production = Pool().get('production')
        productions = Production.search([('origin', '=', str(self))])
        return [production.id for production in productions]

    @classmethod
    def _get_origin(cls):
        origins = super()._get_origin()
        return origins | {'production'}

    def _compute_unit_cost(self, output):
        pool = Pool()
        Location = pool.get('stock.location')
        quantity = Decimal(output.quantity)
        locations = [l.id for l in (Location.search([('type', '=', 'storage')]))]
        current_stock = Decimal(self._get_total_quantity(self.product, self.effective_date, locations)) - quantity
        # if current_stock < 0:
        # raise ProductionQuantityValue(gettext('production_accounting.msg_quantity_value'))
        if current_stock <= 0:
            cost = self.cost / quantity
        else:
            AverageCost = pool.get('product.average_cost')
            domain = [
                    ('product', '=', self.product.id),
                    ('effective_date', '<=', self.planned_date),
                ]
            average_cost = AverageCost.search(domain, order=[('effective_date', 'DESC')])
            old_cost = average_cost[0].cost_price if len(average_cost) > 0 else 0
            if old_cost > 0:
                cost = ((current_stock * old_cost) + (self.cost)) / (current_stock + quantity)
            else:
                cost = self.cost / quantity
        return round_price(cost)

    def _get_total_quantity(self, product, effective_date, locations):
        res = 0
        context = {
                'location_ids': locations,
                'stock_date_end': effective_date,
            }
        with Transaction().set_context(context):
            res_dict = product._get_quantity(
                [product],
                'quantity',
                locations,
                grouping_filter=([product.id],),
            )
            if res_dict.get(product.id):
                res += res_dict[product.id]
        return res

    def get_total_cost(self, name=None):
        return sum([
            self.material_costs or 0,
            self.indirect_costs or 0,
            self.services_costs or 0,
            self.labour_costs or 0,
        ])

    def get_performance(self, name=None):
        res = Decimal(0)
        if self.bom:
            origin = sum(p.quantity for p in self.bom.outputs) * self.quantity
            result = sum(p.quantity for p in self.outputs)
            if result != 0 and origin != 0:
                res = Decimal(str(round(result * 100 / origin, 2)))
        return res

    def update_product_cost(self, product, new_cost):
        ProductCost = Pool().get('product.cost_price')
        ProductAvgCost = Pool().get('product.average_cost')
        products = ProductCost.search([
            ('product', '=', product.id),
        ])
        ProductCost.write(products, {'cost_price': new_cost})
        values = {
            'product': product.id,
            'effective_date': self.effective_date,
            'cost_price': new_cost,
            'stock_move': self.outputs[0],
        }
        ProductAvgCost.create([values])

    @classmethod
    def get_production_lines(cls, rec, date_, kind, factor):
        """ Get Production Account Move Lines """
        cost_production = []
        output = rec.outputs[0]
        lines = []
        company = Transaction().context.get('company')
        for input in rec.inputs:
            # ! account_id = input.product.account_expense_used.id
            # ! # amount = round_price(input.product.cost_price * Decimal(input.quantity))
            # ! amount = round(input.product.cost_price * Decimal(input.quantity), 2)
            # ! line = {
            # !     'description': input.product.rec_name,
            # !     'account': account_id,
            # !     'debit': 0,
            # !     'credit': amount,
            # !     'party': company,
            # ! }
            # ! cost_production.append(amount or 0)
            line, amount = cls.get_line_expense(rec, input, company)
            analytic_account = rec.analytic_account_materials or (('' in rec.warehouse._fields) and rec.warehouse.analytic_account)
            cls.set_analytic_lines(line, date_, analytic_account)
            cost_production.append(amount or 0)
            lines.append(line)

        # ! account_id = None
        for cost in rec.costs:
            print(cost.product.name)
            # ! account_id = cost.product.account_expense_used.id
            # ! line_ = {
            # !     'description': cost.product.rec_name,
            # !     'account': account_id,
            # !     'debit': 0,
            # !     'credit': cost.amount,
            # !     'party': company,
            # ! }
            # account_expense = cost.product.template.account_category.account_expense
            line = cls.get_line_cost(cost, company)
            amount = cost.amount
            cls.set_analytic_lines(line, date_, cost.analytic_account)
            lines.append(line)
            cost_production.append(round(cost.amount, 2) or 0)
        output_account_category = output.product.account_category.account_stock
        # !stock_in = {
        # !    'description': output.product.rec_name,
        # !    'account': output_account_category.id if output_account_category else None,
        # !    'debit': sum(cost_production),
        # !    'credit': 0,
        # !    'party': company,
        # !}
        print(output_account_category.name)
        stock_in = cls.get_consumption_line(rec, output.product, output_account_category, round(sum(cost_production), 2), 0, company)
        print('despues de stock_in')
        lines.append(stock_in)
        return lines

    @classmethod
    def get_line_expense(cls, production, input, company):
        account_id = input.product.account_expense_used.id
        amount = round(input.unit_price * Decimal(input.quantity), 2)
        line = {
            'description': input.product.rec_name,
            'account': account_id,
            'debit': 0,
            'credit': amount,
            'party': company,
        }
        return line, amount

    @classmethod
    def get_line_cost(cls, cost, company):
        account_id = cost.product.account_expense_used.id
        line = {
            'description': cost.product.rec_name,
            'account': account_id,
            'debit': 0,
            'credit': round(cost.amount, 2),
            'party': company,
        }
        return line

    @classmethod
    def get_production_lines_partial(cls, rec, date_, factor, kind=None):
        """ Get Production Account Move Lines Partial """
        cost_production = []
        output = rec.bom.outputs[0]
        lines = []
        company = Transaction().context.get('company')

        account_id = None
        for cost in rec.costs:
            if cost.account_move:
                continue
            account_id = cost.product.account_expense_used.id
            if not cost.amount:
                continue
            line_ = {
                'description': cost.product.rec_name,
                'account': account_id,
                'debit': 0,
                'credit': cost.amount or 0,
                # 'party': company,
            }
            cls.set_analytic_lines(line_, date_, cost.analytic_account)
            lines.append(line_)
            cost_production.append(cost.amount or 0)

        account_partial_stock = rec.account_production.id
        stock_in = {
            'description': output.product.rec_name,
            'account': account_partial_stock,
            'debit': sum(cost_production),
            'credit': 0,
            # 'party': company,
        }
        lines.append(stock_in)
        return lines

    @classmethod
    def get_consumption_lines(cls, record, factor, date_, args=None):
        """ Get Consumption Account Move Lines """
        lines = []
        costs_lines = {}
        company = Transaction().context.get('company')
        for _in in record.inputs:
            product = _in.product
            if product.cost_price == 0:
                continue
            category = product.template.account_category
            if not category or not category.account_stock:
                raise UserError(gettext(
                        'production_accounting.msg_category_account_stock',
                        product=product.rec_name,
                    ))
            account_stock_id = category.account_stock.id
            # credit = round_dec(product.cost_price * Decimal(_in.quantity) * Decimal(factor))
            # credit = round(credit, 0)
            credit = round(_in.unit_price * Decimal(_in.quantity), 2)
            _line = cls.get_consumption_line(record, product, account_stock_id, 0, credit, company)
            lines.append(_line)
            account_expense_id = category.account_expense.id
            try:
                costs_lines[account_expense_id].append(credit)
            except Exception:
                costs_lines[account_expense_id] = [credit]

        material_costs = []
        for account_id, ldebit in costs_lines.items():
            debit = sum(ldebit)
            material_costs.append(debit)
            line_ = cls.get_consumption_line(record, product, account_id, debit, 0, company)
            analytic = args.get('analytic', None)
            if analytic:
                cls.set_analytic_lines(line_, date_, analytic)
            lines.append(line_)

        values = {'material_costs': round_dec(sum(material_costs))}
        return lines, values

    @classmethod
    def get_consumption_line(cls, record, product, account, debit, credit, company):
        line_ = {
                'description': product.template.name,
                'account': account,
                'debit': debit,
                'credit': credit,
                'party': company,
            }
        return line_

    def warning_by_account_move(self, production):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        key = production.number
        if Warning.check(key):
            raise ProductionForceDraftAccountMoves(
                    production.number,
                    gettext('production_accounting.msg_changes_in_account_move'),
                )

    @classmethod
    def create_account_move(cls, records, kind, field=None):
        pool = Pool()
        Move = pool.get('account.move')
        Journal = pool.get('account.journal')
        Period = pool.get('account.period')

        journals = Journal.search([
            ('code', '=', 'STO'),
        ])

        if journals:
            journal = journals[0]
        for rec in records:
            if not rec.planned_date:
                raise UserError(
                    gettext('production_accounting.msg_planned_date_required'),
                )
            output = rec.outputs[0]
            factor = rec.quantity / output.quantity
            lines = None
            if kind == 'wait':
                if rec.in_account_move:
                    rec.warning_by_account_move(rec)
                    continue
                date_ = rec.planned_date
                dict_args = {}
                dict_args['analytic'] = rec.analytic_account_materials or (('' in rec.warehouse._fields) and rec.warehouse.analytic_account)
                lines, values = cls.get_consumption_lines(
                    rec, factor, date_, dict_args,
                )
                # to_update['material_costs'] = values['material_costs']

            if kind == 'done':
                if rec.out_account_move:
                    rec.warning_by_account_move(rec)
                    continue
                date_ = rec.effective_date
                lines = cls.get_production_lines(rec, date_, 'out', factor)
                # lines_in = cls.get_production_lines(rec, date_, 'in', factor)
            if kind == 'partial':
                date_ = date.today()
                lines = cls.get_production_lines_partial(rec, date_, factor, kind)

            if lines:
                period_id = Period.find(rec.company.id, date=date_)
                move, = Move.create([{
                    'journal': journal.id,
                    'period': period_id,
                    'date': date_,
                    'state': 'draft',
                    'lines': [('create', lines)],
                    'origin': str(rec),
                    'description': rec.number,
                }])
                Move.post([move])
                if kind == 'wait':
                    cls.write([rec], {'in_account_move': move.id})
                elif kind == 'done':
                    cls.write([rec], {'out_account_move': move.id})
                # for cost in rec.costs:
                #     if cost.account_move:
                #         continue
                #     cost.account_move = move.id
                #     cost.save()

    def create_stock_move(self, kind, field=None):
        pool = Pool()
        Move = pool.get('stock.move')
        Company = Pool().get('company.company')
        company = Company(Transaction().context.get('company'))
        to_create = []
        warehouse_pdc = self.warehouse.storage_location.id
        from_warehouse = self.warehouse_origin
        if kind == 'in':
            to_warehouse = self.warehouse
            records = self.inputs
            date_ = self.planned_date
        elif kind == 'out':
            to_warehouse = self.warehouse_target
            records = self.outputs
            date_ = self.effective_date

        from_location_id = from_warehouse.storage_location.id
        if to_warehouse:
            to_location_id = to_warehouse.storage_location.id
        else:
            to_location_id = self.warehouse.storage_location.id
        for rec in records:
            to_create.append({
                'product': rec.product,
                'quantity': rec.quantity,
                'from_location': from_location_id,
                'to_location': to_location_id,
                'origin': str(self),
                'effective_date': date_,
                'unit_price': rec.unit_price,
                'unit': rec.unit.id,
                'state': 'draft',
                'currency': company.currency,
            })

        moves = Move.create(to_create)
        Move.do(moves)

    def create_stock_move_sub(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Company = Pool().get('company.company')
        company = Company(Transaction().context.get('company'))
        # to_create = []
        moves = []
        records = self.subs
        from_warehouse = self.warehouse
        to_warehouse = self.location

        from_location_id = from_warehouse.storage_location.id
        to_location_id = to_warehouse.id
        AverageCost = Pool().get('product.average_cost')
        for sub in records:
            date_ = sub.effective_date
            for input in sub.inputs:
                domain = [
                    ('product', '=', input.product.id),
                    ('effective_date', '<=', date_),
                ]
                average_cost = AverageCost.search(domain, order=[('effective_date', 'DESC')])
                if not input.move:
                    move = Move(
                        product=input.product,
                        quantity=float(str(round(input.quantity))),
                        from_location=from_location_id,
                        to_location=to_location_id,
                        origin=str(self),
                        effective_date=date_,
                        unit_price=average_cost[0].cost_price if len(average_cost) > 0 else input.product.cost_price,
                        unit=input.product.default_uom.id,
                        state='draft',
                        currency=company.currency,
                    )
                    # to_create.append(move)
                    Move.save([move])
                    moves.append(move)
                    input.move = move.id
                    input.save()
        # moves = Move.save(to_create)
        Move.do(moves) if len(moves) > 0 else None

    @classmethod
    def set_analytic_lines(cls, line, date, analytic_account):
        "Yield analytic lines for the accounting line and the date"
        if not analytic_account:
            return

        lines = []
        amount = line['debit'] or line['credit']
        for account, amount in analytic_account.distribute(amount):
            analytic_line = {}
            analytic_line['debit'] = amount if line['debit'] else Decimal(0)
            analytic_line['credit'] = amount if line['credit'] else Decimal(0)
            analytic_line['account'] = account
            analytic_line['date'] = date
            lines.append(analytic_line)
        line['analytic_lines'] = [('create', lines)]

    @fields.depends('inputs', 'material_costs')
    def on_change_inputs(self, name=None):
        res = Decimal(0)
        for _input in self.inputs:
            quantity = _input.quantity if _input.quantity else 0
            cost_price = _input.product.cost_price if _input.product else 0
            res += (Decimal(str(quantity)) * cost_price)
        self.material_costs = round_price(res)

    @fields.depends(
        'costs', 'indirect_costs', 'services_costs', 'labour_costs',
        methods=['on_change_inputs'])
    def on_change_costs(self, name=None):
        indirect_costs = []
        services_costs = []
        labour_costs = []
        for cost in self.costs:
            if cost.kind == 'indirect':
                indirect_costs.append(cost.amount or 0)
            elif cost.kind == 'labour':
                labour_costs.append(cost.amount or 0)
            elif cost.kind == 'service' or cost.kind == 'outsourcing':
                services_costs.append(cost.amount or 0)
        self.on_change_inputs()
        self.indirect_costs = sum(indirect_costs)
        self.services_costs = sum(services_costs)
        self.labour_costs = sum(labour_costs)

    def create_cost(self):
        ProductionCost = Pool().get('production.cost')
        direct_costs = []
        for direct_cost in self.bom.direct_costs:
            cost = ProductionCost(
                product=direct_cost.product,
                unit=direct_cost.unit,
                supplier=direct_cost.supplier,
                effective_date=None,
                quantity=direct_cost.quantity * (self.quantity or 0),
                unit_price=direct_cost.unit_price,
                amount=round_price(direct_cost.unit_price * Decimal(direct_cost.quantity) * Decimal(self.quantity or 0)),
                analytic_account=direct_cost.analytic_account,
                kind=direct_cost.kind,
            )
            direct_costs.append(cost)
        return direct_costs

    @classmethod
    def create_purchase(cls, productions):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        purchases = []
        purchase_lines = []
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        draft_productions = [p for p in productions if p.state == 'draft']
        try:
            purchase_date = min(p.planned_start_date or p.planned_date
                                for p in draft_productions if p.planned_date)
        except ValueError:
            purchase_date = today
            if purchase_date < today:
                purchase_date = today
        for production in draft_productions:
            for cost in production.costs:
                if cost.kind == 'outsourcing':
                    purchase_filter = Purchase.search([('party', '=', cost.supplier),
                                                       ('purchase_date', '=', cost.effective_date)])
                    if not len(purchase_filter) > 0:
                        supplier = cost.supplier
                        purchase = Purchase(
                            company=production.company,
                            party=supplier,
                            payment_term=supplier.supplier_payment_term,
                            warehouse=production.warehouse.id,
                            currency=production.company.currency.id,
                            purchase_date=cost.effective_date,
                        )
                        purchase_filter.append(purchase)
                        purchases.append(purchase)
                    purchase_line = PurchaseLine()
                    purchase_line.product = cost.product
                    purchase_line.on_change_product()
                    purchase_line.unit = cost.unit
                    purchase_line.quantity = cost.quantity
                    purchase_line.purchase = purchase_filter[0]
                    purchase_line.compute_taxes(purchase_filter[0].party)
                    purchase_line.unit_price = cost.unit_price
                    purchase_lines.append(purchase_line)
                    cost.purchase_line = purchase_line
                    cost.save()
        Purchase.save(purchases)
        PurchaseLine.save(purchase_lines)

    @fields.depends('planned_date', 'costs', methods=['on_change_costs'])
    def explode_bom(self):
        super(Production, self).explode_bom()
        if self.bom:
            costs = self.create_cost()
            self.costs = costs
            self.on_change_costs()


class ProductionCost(ModelSQL, ModelView):
    "Production Cost"
    __name__ = "production.cost"
    production = fields.Many2One('production', 'Production', required=True,
        ondelete='CASCADE')
    product = fields.Many2One('product.product', 'Product', required=True,
        domain=[('type', '!=', 'active'), ('purchasable', '=', True)])
    product_uom_category = fields.Function(
        fields.Many2One('product.uom.category', 'Product Uom Category'),
        'on_change_with_product_uom_category')
    unit = fields.Many2One('product.uom', 'UoM',
        domain=[
            If(Bool(Eval('product_uom_category')),
                ('category', '=', Eval('product_uom_category')),
                ('category', '!=', -1)),
            ],
        depends=['product', 'product_uom_category'])
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_unit_digits')
    effective_date = fields.Date('Effective Date')
    quantity = fields.Float('Quantity', required=True, digits=(16, Eval('unit_digits', 2)),
        depends=['unit_digits'])
    unit_price = fields.Numeric('Unit Price', required=True, digits=price_digits)
    amount = fields.Numeric('Amount', digits=price_digits,
        states={
            'readonly': True,
        })
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1)),
            # ('parent', '=', Eval('_parent_production', {}).get('analytic_account')),
        ])
    kind = fields.Selection([
        ('labour', 'Labour'),
        ('indirect', 'Indirect'),
        ('outsourcing', 'Outsourcing'),
        ('service', 'Service'),
        ], 'Kind', required=True)
    notes = fields.Text('Notes')
    # account_move = fields.Many2One('account.move', 'Account Move',
    #     states={'readonly': True})
    supplier = fields.Many2One('party.party', 'Supplier',
        states={'invisible': ~Eval('kind').in_(['outsourcing']),
        'required': Eval('kind').in_(['outsourcing'])})
    purchase_line = fields.Many2One('purchase.line', 'Purchase Line',
        states={'invisible': ~Eval('kind').in_(['outsourcing', 'service'])},
        domain=[
            ('purchase.company', '=', Eval('context', {}).get('company', -1)),
            ])

    @classmethod
    def __setup__(cls):
        super(ProductionCost, cls).__setup__()
        cls._buttons.update({
            'create_purchase': {
                'invisible': ~Eval('kind').in_(['outsourcing', 'service']),
            },
        })

    @fields.depends('kind', 'product', 'purchase_line', 'unit', 'unit_price')
    def on_change_kind(self, name=None):
        if self.kind != 'outsourcing':
            self.unit = None
            self.unit_price = None
            self.purchase_line = None
            self.product = None

    @fields.depends('product', 'unit', 'unit_price', 'unit_digits')
    def on_change_product(self, name=None):
        if self.product:
            category = self.product.default_uom.category
            if not self.unit or self.unit.category != category:
                self.unit = self.product.default_uom.id
                self.unit_digits = self.product.default_uom.digits
            self.unit_price = self.product.cost_price

    @fields.depends('unit_price', 'amount', 'quantity')
    def on_change_with_amount(self, name=None):
        if self.unit_price and self.quantity:
            return round(self.unit_price * Decimal(self.quantity), 2)

    @fields.depends('unit')
    def on_change_with_unit_digits(self, name=None):
        if self.unit:
            return self.unit.digits
        return 2

    @fields.depends('product')
    def on_change_with_product_uom_category(self, name=None):
        if self.product:
            return self.product.default_uom_category.id

    @classmethod
    @ModelView.button
    def create_purchase(cls, records):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        for record in records:
            if record.production.state == 'done' or record.purchase_line:
                break
            purchase = Purchase(
                company=record.production.company,
                party=record.supplier,
                payment_term=record.supplier.supplier_payment_term,
                warehouse=record.production.warehouse.id,
                currency=record.production.company.currency.id,
                purchase_date=date.today(),
            )
            purchase_line = PurchaseLine()
            purchase_line.product = record.product
            purchase_line.unit = record.unit
            purchase_line.unit_price = record.unit_price
            purchase_line.quantity = record.quantity
            purchase_line.purchase = purchase
            purchase_line.on_change_product()
            Purchase.save([purchase])
            PurchaseLine.save([purchase_line])
            record.purchase_line = purchase_line
            record.save()

    @classmethod
    def delete(cls, records):
        for record in records:
            if record.purchase_line and record.purchase_line.purchase.state == 'draft':
                record.purchase_line.delete([record.purchase_line])
        super(ProductionCost, cls).delete(records)


class ProductionReport(CompanyReport):
    "Production Report"
    __name__ = 'production.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        total_inputs = {}
        company_id = Transaction().context.get('company')
        Company = Pool().get('company.company')
        company = Company(company_id)
        productions = []
        for record in records:
            productions.append(record.number)
            for input in record.inputs:
                product_name = input.product.name
                try:
                    total_inputs[product_name]['quantity'] += input.quantity
                except KeyError:
                    total_inputs[product_name] = {
                        'name': product_name,
                        'quantity': input.quantity,
                        'unit': input.product.default_uom.name,
                    }
        report_context['records'] = records
        report_context['productions'] = productions
        report_context['company'] = company
        report_context['date_today'] = date.today()
        report_context['moves'] = total_inputs.values()
        return report_context


class DoneProductions(Wizard):
    "Done Productions"
    __name__ = 'production.done_productions'
    start_state = 'done_productions'
    done_productions = StateTransition()

    @classmethod
    def __setup__(cls):
        super(DoneProductions, cls).__setup__()

    def transition_done_productions(self):
        Production = Pool().get('production')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'

        productions = Production.browse(ids)
        for prd in productions:
            if prd.state != 'running':
                continue
            Production.done([prd])

        return 'end'


class ProcessProductionAsyncStart(ModelView):
    "Process Production Async Start"
    __name__ = 'production.process_production_async.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    date = fields.Date('Start Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ProcessProductionAsync(Wizard):
    "Process Production Async"
    __name__ = 'production.process_production_async'
    start = StateView(
        'production.process_production_async.start',
        'production_accounting.process_production_async_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def create_move(self, lines, journal, period_id):
        Move = Pool().get('account.move')
        move, = Move.create([{
            'journal': journal.id,
            'period': period_id,
            'date': self.start.date,
            'state': 'draft',
            'lines': [('create', lines)],
            'description': '',
        }])
        Move.post([move])
        return move

    def update_product_cost(self, producibles, outputs):
        ProductAvgCost = Pool().get('product.average_cost')
        Date = Pool().get('ir.date')
        for product, value in producibles.items():
            cost_price = value['bom'].compute_unit_cost()
            product_id = product.id
            move = None
            for output in outputs:
                if output.product.id == product_id:
                    move = output.id
                    outputs.remove(output)
                    break
            values = {
                'product': product.id,
                'effective_date': Date.today(),
                'cost_price': cost_price,
                'stock_move': move,
            }
            ProductAvgCost.create([values])

    def get_analytic_account(self, product, shop):
        if not hasattr(product, 'analytic_accounts') and shop.warehouse.storage_location.analytic_account:
            return shop.warehouse.storage_location.analytic_account
        for analytic_account in product.analytic_accounts:
            if analytic_account.shop.id == shop.id and analytic_account.kind == 'expenses':
                return analytic_account.analytic_account

    def group_by_account(self, inputs, output, company, factor):
        lines_input_move = []
        lines_output_move = []
        account_expense_output = output.product.account_expense_used
        account_stock_output = output.product.account_stock_used
        for input in inputs:
            _product = input.product
            account_expense_input = _product.account_expense_used
            account_stock_input = _product.account_stock_used
            input_amount = Decimal(
                input.quantity * float(_product.cost_price) * factor,
            )
            try:
                accounts_stock[account_stock_input].append(input_amount)
                accounts_expense[account_expense_input]['amounts'].append(input_amount)
            except KeyError:
                lines_input_move[account_stock_input] = {
                    'description': '',
                    'account': account_stock_input.id,
                    'debit': 0,
                    'credit': input_amount,
                    'party': company,
                }
                lines_input_move[account_expense_input] = {
                    'description': '',
                    'account': account_expense_input.id,
                    'debit': input_amount,
                    'credit': 0,
                    'party': company,
                }
                lines_output_move[account_expense] = {
                    'description': '',
                    'account': account_expense.id,
                    'debit': input_amount,
                    'credit': 0,
                    'party': company,
                }
                lines_output_move[account_expense] = {
                    'description': '',
                    'account': account_expense.id,
                    'debit': input_amount,
                    'credit': 0,
                    'party': company,
                }

    def manage_dict_moves(self, date_, input, dict_input, dict_output, factor):
        product = input.product
        account_expense = product.account_expense_used
        account_stock = product.account_stock_used
        analytic = self.get_analytic_account(product, self.start.shop)
        input_amount = Decimal(
                input.quantity * float(product.cost_price) * factor,
            ).quantize(Decimal('0.01'))
        try:
            dict_input[account_stock]['credit'] += input_amount
        except KeyError:
            dict_input[account_stock] = {
                    'description': '',
                    'account': account_stock.id,
                    'debit': 0,
                    'credit': input_amount,
                    'party': self.start.company.party.id,
                }
        try:
            dict_input[account_expense]['debit'] += input_amount
        except KeyError:
            dict_input[account_expense] = {
                    'description': '',
                    'account': account_expense.id,
                    'debit': input_amount,
                    'credit': 0,
                    'party': self.start.company.party.id,
                    'analytic_state': 'valid',
                }
            Production.set_analytic_lines(dict_input[account_expense], date_, analytic)
        try:
            dict_output[account_expense]['credit'] += input_amount
        except KeyError:
            dict_output[account_expense] = {
                    'description': '',
                    'account': account_expense.id,
                    'debit': 0,
                    'credit': input_amount,
                    'party': self.start.company.party.id,
                    'analytic_state': 'valid',
                }
            Production.set_analytic_lines(dict_output[account_expense], date_, analytic)
        return input_amount

    def create_stock_moves(self, producibles, company):
        Move = Pool().get('stock.move')
        date_ = self.start.date
        in_moves = []
        out_moves = []
        from_location_id = self.start.shop.warehouse.storage_location.id
        to_location_id = self.start.shop.warehouse.production_location.id
        AverageCost = Pool().get('product.average_cost')
        for product, values in producibles.items():
            output = values['bom'].outputs[0]
            quantity = round(sum(values['quantity']), product.template.default_uom.digits)
            if quantity <= 0:
                continue
            factor = quantity / output.quantity
            for input in values['bom'].inputs:
                _product = input.product
                domain = [('product', '=', _product.id), ('effective_date', '<=', self.start.date)]
                average_cost = AverageCost.search(domain, order=[('effective_date', 'DESC')])
                qty = round(
                    input.quantity * factor,
                    _product.template.default_uom.digits,
                )
                in_moves.append({
                    'product': _product.id,
                    'unit': _product.template.default_uom.id,
                    'effective_date': date_,
                    'quantity': qty,
                    'unit_price': average_cost[0].cost_price if len(average_cost) > 0 else _product.cost_price,
                    'from_location': from_location_id,
                    'to_location': to_location_id,
                    'currency': company.currency,
                })
            out_moves.append({
                'product': product.id,
                'unit': product.template.default_uom.id,
                'effective_date': date_,
                'quantity': quantity,
                'unit_price': product.cost_price,
                'from_location': to_location_id,
                'to_location': from_location_id,
                'currency': company.currency,
            })
        res1 = Move.create(in_moves)
        res2 = Move.create(out_moves)
        Move.do(res1 + res2)
        return res2

    def create_production_moves(self, producibles, journal, period_id):
        # Production = Pool().get('production')
        # analytic = self.start.shop.analytic_account if hasattr(self.start.shop, 'analytic_account') else None
        date_ = self.start.date
        # company = self.start.company
        dict_input = {}
        dict_output = {}
        # analytic_lines = {}
        AnalyticLine = Pool().get('analytic_account.line')
        for product, values in producibles.items():
            quantity = sum(values['quantity'])
            if quantity <= 0:
                continue
            bom = values['bom']
            output = bom.outputs[0]
            inputs = bom.inputs
            factor = quantity / output.quantity
            output_amount = 0
            for input in inputs:
                output_amount += self.manage_dict_moves(date_, input, dict_input, dict_output, factor)
            try:
                dict_output[output.product.account_stock_used]['debit'] += output_amount
            except KeyError:
                dict_output[output.product.account_stock_used] = {
                    'description': '',
                    'account': output.product.account_stock_used.id,
                    'debit': output_amount,
                    'credit': 0,
                    'party': self.start.company.party.id,
                }
        input_move = self.create_move(dict_input.values(), journal, period_id)
        analytic_lines = []
        for input_move in input_move.lines:
            for analytic_line in input_move.analytic_lines:
                analytic_line.debit = input_move.debit
                analytic_line.credit = input_move.credit
                analytic_lines.append(analytic_line)
        output_move = self.create_move(dict_output.values(), journal, period_id)
        for output_move in output_move.lines:
            for analytic_line in output_move.analytic_lines:
                analytic_line.debit = output_move.debit
                analytic_line.credit = output_move.credit
                analytic_lines.append(analytic_line)
        AnalyticLine.save(analytic_lines)
        # material_costs = []
        # for acc, amount in accounts_stock.items():
        #     amount = Decimal(round(sum(amount), 2))
        #     line_ = {
        #         'description': '',
        #         'account': acc.id,
        #         'debit': 0,
        #         'credit': amount,
        #         'party': company
        #     }
        #     lines.append(line_)
        #     # material_costs.append(amount)

        # # material_costs = sum(material_costs)
        # for acc, value in accounts_expense.items():
        #     amount = Decimal(round(sum(value['amounts']), 2))

        #     line_ = {
        #         'description': '',
        #         'account': acc.id,
        #         'debit': amount,
        #         'credit': 0,
        #         'party': company,
        #     }
        #     Production.set_analytic_lines(line_, date_, value['analytic'])
        #     lines.append(line_)
        # self.create_move(lines, journal, period_id)

        # # Second move for load stock and discharge production
        # lines2 = []
        # dc_amount = []
        # for dc in bom.direct_costs:
        #     account_id = dc.product.account_expense_used.id
        #     amount = Decimal(dc.quantity) * dc.product.cost_price
        #     amount = Decimal(round(float(amount) * factor, 2))
        #     dc_amount.append(amount)
        #     line_ = {
        #         'description': '',
        #         'account': account_id,
        #         'debit': 0,
        #         'credit': amount,
        #         'party': company
        #     }
        #     if not dc.analytic_account:
        #         raise UserError('No se han configurado las cuentas analiticas en la lista ' + dc.name)
        #     Production.set_analytic_lines(line_, date_, dc.analytic_account)
        #     lines2.append(line_)
        # line_ = {
        #     'description': '',
        #     'account': account_expense.id,
        #     'debit': 0,
        #     'credit': material_costs,
        #     'party': company
        # }
        # material_costs = 0
        # table = Table(title="Todo List")
        # table.add_column("cuenta", style="cyan", no_wrap=True)
        # table.add_column("debe", style="magenta")
        # table.add_column("haber", justify="right", style="green")
        # for acc, value in accounts_expense_two.items():
        #     amount = Decimal(round(sum(value['amounts']), 2))
        #     line_ = {
        #         'description': '',
        #         'account': acc.id,
        #         'debit': 0,
        #         'credit': amount,
        #         'party': company
        #     }
        #     Production.set_analytic_lines(line_, date_, value['analytic'])
        #     table.add_row(acc.name, '0', str(amount))
        #     lines2.append(line_)
        # # stock_amount = material_costs + sum(dc_amount)
        # # line_ = {
        # #     'description': '',
        # #     'account': output.product.account_stock_used.id,
        # #     'debit': stock_amount,
        # #     'credit': 0,
        # #     'party': company
        # # }
        # # Production.set_analytic_lines(line_, date_, analytic)
        # for acc, amount in accounts_stock_two.items():
        #     amount = Decimal(round(sum(amount), 2))
        #     line_ = {
        #         'description': '',
        #         'account': acc.id,
        #         'debit': amount,
        #         'credit': 0,
        #         'party': company
        #     }
        #     lines2.append(line_)
        #     table.add_row(acc.name, str(amount), '0')
        # console = Console()
        # console.print(table)
        # self.create_move(lines2, journal, period_id)

    def transition_accept(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        Period = pool.get('account.period')
        Journal = pool.get('account.journal')
        Configuration = pool.get('production.configuration')
        configuration = Configuration(1)
        company = self.start.company
        journals = Journal.search([
            ('code', '=', 'STO'),
        ])

        if journals:
            journal = journals[0]

        period_id = Period.find(company.id, date=self.start.date)

        BOMOutput = pool.get('production.bom.output')
        dom = [
            ('sale.state', 'in', ['processing', 'done', 'transferred']),
            ('sale.shop', '=', self.start.shop.id),
            ('sale.sale_date', '=', self.start.date),
            ('product.template.producible', '=', True),
            ('type', '=', 'line'),
            ('produced', '=', False),
        ]
        if 'origin' in SaleLine._fields:
            # dom.append(('origin', 'not like', 'sale.line,%'))
            pass
        lines = SaleLine.search(dom)
        if not lines:
            return 'end'

        producibles = {}

        def _add_producibles(product, qty, sub=False):
            try:
                producibles[product]['quantity'].append(qty)
                bom = producibles[product]['bom']
                output = bom.outputs[0]
            except:
                outputs = BOMOutput.search([
                    ('product', '=', product.id),
                    ('bom.active', '=', True),
                    ('bom.not_synchronize_sales', '=', False),
                ])
                if not outputs:
                    return
                bom = outputs[0].bom
                if sub and not bom.is_subproduction:
                    return
                output = outputs[0]
                producibles[product] = {
                    'quantity': [qty],
                    'bom': bom,
                }
            # Here we add all subproducts inside inputs too, because is
            # neccesary for complete its production
            for input in bom.inputs:
                in_quantity, in_product = input.quantity, input.product
                if in_product.template.producible:
                    _add_producibles(
                        in_product,
                        in_quantity * (qty / output.quantity),
                        sub=True)

        for line in lines:
            _add_producibles(line.product, line.quantity)
        print('validate ingresa a este metodo -------------------------')
        if configuration.production_accounting:
            self.create_production_moves(producibles, journal, period_id)

        outputs = self.create_stock_moves(producibles, company)
        print('validate ingresa a este metodo 2-------------------------')
        self.update_product_cost(producibles, outputs)
        SaleLine.write(lines, {'produced': True})
        LogProductionAsync = pool.get('production_accounting.log_production')
        current_user_id = Transaction().user
        log_production_sync = LogProductionAsync(
            user=current_user_id,
            shop=self.start.shop.id,
            date_executed=self.start.date,
            execute_date_wizard=date.today(),
        )
        log_production_sync.save()
        return 'end'


class ProductionDetailedStart(ModelView):
    "Production Detailed Start"
    __name__ = 'production.detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    grouped = fields.Boolean('Grouped', help='Grouped by products')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class ProductionDetailed(Wizard):
    "Production Detailed"
    __name__ = 'production.detailed'
    start = StateView(
        'production.detailed.start',
        'production_accounting.production_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('production.detailed_report')

    def do_print_(self, action):
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'grouped': self.start.grouped,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ProductionDetailedReport(Report):
    "Production Detailed Report"
    __name__ = 'production.detailed_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Production = Pool().get('production')
        states_dic = {
            'request': 'Solicitud',
            'draft': 'Borrador',
            'waiting': 'Espera',
            'assigned': 'Reservado',
            'running': 'Ejecución',
            'done': 'Finalizada',
            'cancelled': 'Cancelada',
        }
        start_datetime = datetime.combine(data['start_date'], datetime.min.time())
        end_datetime = datetime.combine(data['end_date'], datetime.max.time())
        domain = [
            ('company', '=', data['company']),
            ('create_date', '>=', start_datetime),
            ('create_date', '<=', end_datetime),
            ]
        fields_names = ['warehouse.name', 'location.name', 'effective_date',
            'number', 'uom.name', 'quantity', 'cost', 'product.name', 'product.id',
            'effective_start_date', 'state', 'inputs.product.name', 'inputs.product.code', 'inputs.quantity',
            'inputs.effective_date']
        productions = Production.search_read(domain, fields_names=fields_names)
        if not data['grouped']:
            records = productions
        else:
            records = {}
            for p in productions:
                key = str(p['product.']['id']) + p['location.']['name']
                try:
                    records[key]['quantity'] += p['quantity']
                    records[key]['cost'] += p['cost']
                except:
                    records[key] = p
                    records[key]['effective_date'] = None
                    records[key]['effective_start_date'] = None
                    records[key]['numero'] = None
            records = records.values() if records else []
        report_context['records'] = records
        report_context['Decimal'] = Decimal
        report_context['states_dic'] = states_dic
        return report_context


class ProductionForceDraft(Wizard):
    "Production Force Draft"
    __name__ = 'production.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def _reset_production(self, prod, stock_move, cursor):
        # stock_move = Table('stock_move')
        # cursor = Transaction().connection.cursor()
        for rec in prod.outputs:
            cursor.execute(*stock_move.update(
                columns=[stock_move.state],
                values=['draft'],
                where=stock_move.id == rec.id),
            )
        inputs = [rec.id for rec in prod.inputs]
        if inputs:
            cursor.execute(*stock_move.update(
                columns=[stock_move.state],
                values=['draft'],
                where=stock_move.id.in_(inputs)),
            )
        prod.state = 'draft'
        prod.save()

    def _reset_subproduction(self, subprod, stock_move, cursor):
        inputs = [rec.move.id if rec.move else None for rec in subprod.inputs]
        if inputs:
            cursor.execute(*stock_move.update(
                columns=[stock_move.state],
                values=['draft'],
                where=stock_move.id.in_(inputs)),
            )

    def transition_force_draft(self):
        # id_ = Transaction().context['active_id']
        ids = Transaction().context['active_ids']
        Production = Pool().get('production')
        stock_move = Table('stock_move')
        cursor = Transaction().connection.cursor()
        if ids:
            productions = Production.search([
                    ('id', 'in', ids),
                    ('state', '!=', 'draft'),
                ])
            for prod in productions:
                self._reset_production(prod, stock_move, cursor)
                self.delete_account_moves(prod)
                self.delete_average_cost(prod)
                for sub in prod.subs:
                    self._reset_subproduction(sub, stock_move, cursor)
        return 'end'

    def delete_account_moves(self, production):
        pool = Pool()
        Reconciliation = pool.get('account.move.reconciliation')
        AccountMove = pool.get('account.move')
        account_moves = AccountMove.search([
                ('origin', '=', str(production)),
                ('period.state', '=', 'open')
            ])
        if len(account_moves) == 0:
            raise ProductionAccountMove(
                    gettext('production_accounting.msg_account_move'))
        cursor = Transaction().connection.cursor()
        reconciliations = []
        for move in account_moves:
            reconciliations.extend([
                l.reconciliation for l in move.lines if l.reconciliation
            ])
        if reconciliations:
            Reconciliation.delete(reconciliations)
        for move in account_moves:
            cursor.execute("DELETE FROM account_move WHERE id=%s", (move.id,))

    def delete_average_cost(self, production):
        AverageCost = Pool().get('product.average_cost')
        if production.outputs:
            domain = [('stock_move', '=', production.outputs[0].id)]
            average_cost = AverageCost.search(domain)
            average_cost[0].delete([average_cost[0]]) if len(average_cost) > 0 else None


# class UpdateCostProductionStart(ModelView):
#     """Update Cost Production Start"""
#     __name__ = 'production_accounting.update_cost_production.start'
#     effective_date = fields.Date('Effect. Date', required=True)
#     products = fields.Many2Many('product.product', None, None, 'Product',
#                                domain=[('producible', '=', True)])


class UpdateCostProduction(Wizard):
    "Update Cost Production"
    __name__ = 'production_accounting.update_cost_production'

    # start = StateView('production_accounting.update_cost_production.start',
    #                   'production_accounting.update_cost_production_start_view_form', [
    #                       Button('Cancel', 'end', 'tryton-cancel'),
    #                       Button('Update', 'update_productions',
    #                              'tryton-ok', default=True),
    #                       ])
    start_state = 'update_productions'
    update_productions = StateTransition()

    def transition_update_productions(self):
        pool = Pool()
        Move = pool.get('stock.move')
        AverageCost = Pool().get('product.average_cost')
        cursor = Transaction().connection.cursor()
        domain = [
            ('production_input', '!=', None),
            # ('state', '=', 'done')
        ]
        inputs = Move.search(domain)
        for input in inputs:
            domain_avg = [
                ('effective_date', '<=', input.effective_date),
                ('product', '=', input.product),
                ]
            average_cost = AverageCost.search(domain_avg, order=[('effective_date', 'DESC'), ('id', 'DESC')])
            if len(average_cost) > 0:
                query = f"""UPDATE stock_move
                                SET unit_price = {average_cost[0].cost_price}
                                WHERE id={input.id}
                        """
                cursor.execute(query)
        return 'end'
