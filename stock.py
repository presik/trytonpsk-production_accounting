# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.model import fields


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @fields.depends('from_location', 'to_location')
    def on_change_with_unit_price_required(self, name=None):
        response = super(Move, self).on_change_with_unit_price_required()
        from_type = self.from_location.type if self.from_location else None
        to_type = self.to_location.type if self.to_location else None
        if from_type == 'storage' and to_type == 'production':
            response = True
        return response

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['production']
