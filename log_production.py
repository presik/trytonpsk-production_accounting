from trytond.model import fields, ModelSQL, ModelView, Workflow
from trytond.pool import PoolMeta

STATES = {
    'required': True,
    'readonly': True
}


class LogProductionAsync(ModelSQL, ModelView):
    """Log Production Async"""
    __name__ = 'production_accounting.log_production'
    user = fields.Many2One('res.user', 'User', states=STATES)
    shop = fields.Many2One('sale.shop', 'Shop', states=STATES)
    date_executed = fields.Date('Date Executed', states=STATES)
    execute_date_wizard = fields.Date('Execute Date Wizard', states=STATES)

    @classmethod
    def delete(cls, records):
        pass
