# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from decimal import Decimal
from trytond.modules.product import price_digits, round_price
from trytond.wizard import (StateTransition, Wizard)
from trytond.transaction import Transaction
from trytond.exceptions import UserError, UserWarning


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    produced = fields.Boolean('Produced', states={'readonly': True})

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('produced', None)
        return super(SaleLine, cls).copy(lines, default)


class CreateProductionFromSale(Wizard):
    'Create  Production'
    __name__ = 'production_accounting.create_production_from_sale'
    start_state = 'create_production'
    create_production = StateTransition()

    def transition_create_production(self):
        pool = Pool()
        ids = Transaction().context['active_ids']
        Sale = pool.get('sale.sale')
        Production = pool.get('production')
        sales = Sale.browse(ids)
        productions_list = []
        for sale in sales:
            if sale.state == 'draft':
                raise UserError('La venta no debe encontrarse no debe encontrarse en borrador')
            Bom = pool.get('production.bom')
            for line in sale.lines:
                if line.product.producible:
                    bom = Bom.search([('output_products', '=', line.product)])
                    if bom:
                        production = {
                            'reference': sale.reference or sale.number,
                            'company': sale.company.id,
                            'warehouse': sale.warehouse.id,
                            'location': sale.warehouse.production_location.id,
                            'product': line.product.id,
                            'uom': line.product.default_uom.id,
                            'quantity': line.quantity,
                            'state': 'draft',
                            'origin': str(line),
                            'bom': bom[0].id if bom else None,
                        }
                    else:
                        raise UserError('No BOM exists', line.product.rec_name)
                    productions_list.append(production)
        productions = Production.create(productions_list)
        for production in productions:
            production.on_change_bom()
            production.save()
        return 'end'
